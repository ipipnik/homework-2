//
//  main.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 04/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//
#import <stdio.h>

#import <Foundation/Foundation.h>
#import "SPIStarSystem.h"
#import "SPISpaceObject.h"

#import "SPIPlanet.h"
#import "SPIAsteroidField.h"
#import "SPIPlayerSpaceship.h"
#import "SPIStar.h"

int main(int argc, const char * argv[]) {    
    
    // Солнечная звездная система (ее часть) (I)
    SPIStarSystem *solarStarSystem = [[SPIStarSystem alloc] initWithName:@"Solar" age:@(230000000)];
    
        // Звезда
    SPIStar* solarStar = [[SPIStar alloc] initWithName:@"Solar"];
    solarStar.massOfStar = @1;
    solarStar.classOfStar = K;
    solarStar.destroyable = YES;
    
        // Планета
    SPIPlanet* mercuryPlanet = [[SPIPlanet alloc] initWithName:@"Mercury"];
    mercuryPlanet.atmosphere = NO;
    mercuryPlanet.destroyable = YES;
    
        // Планета
    SPIPlanet* venusPlanet = [[SPIPlanet alloc] initWithName:@"Venus"];
    venusPlanet.atmosphere = NO;
    venusPlanet.destroyable = YES;
    
        // Планета
    SPIPlanet* earthPlanet = [[SPIPlanet alloc] initWithName:@"Earth"];
    earthPlanet.atmosphere = YES;
    earthPlanet.peoplesCount = 8000000000;
    earthPlanet.destroyable = NO;
    
        // Пояс астероидов
    SPIAsteroidField* ceresAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Ceres"];
    ceresAsteroidField.density = 67889;
    ceresAsteroidField.destroyable = YES;
    
    solarStarSystem.spaceObjects = [[NSMutableArray alloc] initWithArray:@[solarStar, mercuryPlanet, venusPlanet, earthPlanet, ceresAsteroidField]];
    // ---
    
    
    // Другая звездная система (II)
    SPIStarSystem *taygetaStarSystem = [[SPIStarSystem alloc] initWithName:@"Taygeta" age:@(70000000)];
    
        // Планета
    SPIStar* nusakanStar = [[SPIStar alloc] initWithName:@"Nusakan"];
    nusakanStar.massOfStar = @4;
    nusakanStar.classOfStar = O;
    nusakanStar.destroyable = NO;
    
        // Планета
    SPIPlanet* meissaPlanet = [[SPIPlanet alloc] initWithName:@"Meissa"];
    meissaPlanet.atmosphere = YES;
    meissaPlanet.peoplesCount = 100;
    meissaPlanet.destroyable = YES;
    
        // Планета
    SPIPlanet* hekaPlanet = [[SPIPlanet alloc] initWithName:@"Heka"];
    hekaPlanet.atmosphere = NO;
    hekaPlanet.peoplesCount = 35487425;
    hekaPlanet.destroyable = YES;
    
        // Пояс астероидов
    SPIAsteroidField* cursaAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Cursa"];
    cursaAsteroidField.density = 389;
    cursaAsteroidField.destroyable = NO;
    
    taygetaStarSystem.spaceObjects = [[NSMutableArray alloc] initWithArray:@[nusakanStar, meissaPlanet, hekaPlanet, cursaAsteroidField]];
    // ---
    
    // Еще одна звездная система (III)
    SPIStarSystem* auvaStarSystem = [[SPIStarSystem alloc] initWithName:@"Auva" age:@(999999)];
    
        // Звезда
    SPIStar* minelavaStar = [[SPIStar alloc] initWithName:@"Minelava"];
    minelavaStar.massOfStar = @3;
    minelavaStar.classOfStar = A;
    minelavaStar.destroyable = YES;
    
        // Планета
    SPIPlanet* alzirPlanet = [[SPIPlanet alloc] initWithName:@"Alzir"];
    alzirPlanet.atmosphere = YES;
    alzirPlanet.peoplesCount = 1;
    alzirPlanet.destroyable = NO;
    
        // Пояс астероидов
    SPIAsteroidField* alruccabahAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Alruccabah"];
    alruccabahAsteroidField.density = 389;
    alruccabahAsteroidField.destroyable = NO;
    
        // Пояс астероидов
    SPIAsteroidField* naosAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Naos"];
    naosAsteroidField.density = 1598;
    naosAsteroidField.destroyable = NO;
    
    auvaStarSystem.spaceObjects = [[NSMutableArray alloc] initWithArray:@[minelavaStar, alzirPlanet, alruccabahAsteroidField, naosAsteroidField]];
    // ---
    
    
    // Переход между звездными системами. (Возможно не очень красивый, зато простой).
    solarStarSystem.anotherStarSystem = @[taygetaStarSystem, auvaStarSystem];
    taygetaStarSystem.anotherStarSystem  = @[solarStarSystem, auvaStarSystem];
    auvaStarSystem.anotherStarSystem = @[solarStarSystem, taygetaStarSystem];
    // ---
    
    SPIPlayerSpaceship *spaceship = [[SPIPlayerSpaceship alloc] initWithName:@"Titan"];
    
    [spaceship loadStarSystem:solarStarSystem];
    
    BOOL play = YES;
    
    NSMutableArray *gameObjects = [[NSMutableArray alloc] init];
    [gameObjects addObjectsFromArray:solarStarSystem.spaceObjects];
    [gameObjects addObject:spaceship];
    
    while (play) {
        SPIPlayerSpaceshipResponse response = [spaceship waitForCommand];
        
        if (response == SPIPlayerSpaceshipResponseExit) {
            play = NO;
            continue;
        }
        
        for (id<SPIGameObject> gameObject in gameObjects) {
            [gameObject nextTurn];
        }
    }
    return 0;
}
