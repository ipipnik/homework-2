//
//  PlayerSpaceship.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 17/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIPlayerSpaceship.h"

@implementation SPIPlayerSpaceship

- (instancetype)initWithName:(NSString *)name {
    self = [super init];
    if (self) {
        _name = name;
    }
    return self;
}

- (void)loadStarSystem:(SPIStarSystem *)starSystem {
    _starSystem = starSystem;
    _currentSpaceObject = starSystem.spaceObjects.firstObject;
}


- (SPIPlayerSpaceshipResponse)waitForCommand {
    char command[255];
    
    
    printf("\n%s", [[self availableCommands] cStringUsingEncoding:NSUTF8StringEncoding]);
    printf("\n%s ",[@"Input command:" cStringUsingEncoding:NSUTF8StringEncoding]);
    
    fgets(command, 255, stdin);
    int commandNumber = atoi(command);
    self.nextCommand = commandNumber;
    
    return self.nextCommand == SPIPlayerSpaceshipCommandExit ? SPIPlayerSpaceshipResponseExit : SPIPlayerSpaceshipResponseOK;
}

- (void)nextTurn {
    
    switch (self.nextCommand) {
        case SPIPlayerSpaceshipCommandFlyToPreviousObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex > 0) {
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandExploreCurrentObject: {
            printf("%s\n", [[self.currentSpaceObject description] cStringUsingEncoding:NSUTF8StringEncoding]);
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex < (self.starSystem.spaceObjects.count - 1)) {
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex + 1];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToAnotherStarSystem:
        {
            printf("Fly to: \n");
            NSInteger i = 1;
            for(SPIStarSystem *starSystem in self.starSystem.anotherStarSystem)
            {
                printf("%ld. %s \n", i, [starSystem.name cStringUsingEncoding:NSUTF8StringEncoding]);
                i++;
            }
            
            char action[255];
            fgets(action, 255, stdin);
            int actionNumber = atoi(action);
            
            self.starSystem = [self.starSystem.anotherStarSystem objectAtIndex: actionNumber - 1];
            break;
        }
            
        case SPIPlayerSpaceshipCommandDestroySpaceobject:{
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (self.currentSpaceObject.destroyable == YES)
            {
                printf("\n%s is destroyed \n", [self.currentSpaceObject.name cStringUsingEncoding:NSUTF8StringEncoding]);
                
                switch (self.currentSpaceObject.type) {
                    case SPISpaceObjectTypeStar:{
                        SPIPlanet *new = [[SPIPlanet alloc] initWithName:self.currentSpaceObject.name];
                        new.atmosphere = NO;
                        new.peoplesCount = 0;
                        new.destroyable = YES;
                        
                        [self.starSystem.spaceObjects replaceObjectAtIndex:currentSpaceObjectIndex withObject:(SPIPlanet*) new];
                        
                        break;
                    }
                    case SPISpaceObjectTypePlanet:{
                        SPIAsteroidField *new = [[SPIAsteroidField alloc] initWithName:self.currentSpaceObject.name];
                        new.density = ((int)(arc4random() % 100)) - 50;
                        new.destroyable = YES;
                        
                        [self.starSystem.spaceObjects replaceObjectAtIndex:currentSpaceObjectIndex withObject:(SPIAsteroidField*) new];
                        
                        break;
                    }
                    case SPISpaceObjectTypeAsteroidField:{
                        [self.starSystem.spaceObjects removeObjectAtIndex:currentSpaceObjectIndex];
                        
                        break;
                    }
                        
                    default:{
                        break;
                    }
                        
                }
                if ([self.starSystem.spaceObjects count] < currentSpaceObjectIndex + 1 && [self.starSystem.spaceObjects count] != 0) self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1];
                else if ([self.starSystem.spaceObjects count] != 0) self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex];
                else self.currentSpaceObject = nil;
                
            }
            else{
                NSLog(@"\nYou can't destroy this object\n");
            }
            break;
        }
            
        default:{
            break;
        }
    }
    self.nextCommand = 0;
}

- (NSString *)availableCommands {
    if (self.starSystem) {
        NSMutableArray *mutableDescriptions = [[NSMutableArray alloc] init];
        if(self.currentSpaceObject != nil){
            for (NSInteger command = SPIPlayerSpaceshipCommandFlyToPreviousObject; command <= SPIPlayerSpaceshipCommandDestroySpaceobject; command++) {
                NSString *description = [self descriptionForCommand:command];
                if (description) {
                    [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)command, description]];
                }
            }
        }
        else{
            NSLog(@"\nCurrent Star system is Empty!\n");
        }
        NSString* description = [self descriptionForCommand:SPIPlayerSpaceshipCommandFlyToAnotherStarSystem];
        if (description) {
            [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)SPIPlayerSpaceshipCommandFlyToAnotherStarSystem, description]];
        }
        
        description = [self descriptionForCommand:SPIPlayerSpaceshipCommandExit];
        if (description) {
            [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)SPIPlayerSpaceshipCommandExit, description]];
        }
        
        
        return [mutableDescriptions componentsJoinedByString:@"\n"];
    }
    return nil;
}

- (NSString *)descriptionForCommand:(SPIPlayerSpaceshipCommand)command {
    NSString *commandDescription = nil;
    switch (command) {
        case SPIPlayerSpaceshipCommandFlyToPreviousObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex > 0) {
                commandDescription = [NSString stringWithFormat:@"Fly to %@", [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1] title]];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandExploreCurrentObject: {
            commandDescription = [NSString stringWithFormat:@"Explore %@", [self.currentSpaceObject title]];
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex < (self.starSystem.spaceObjects.count - 1)) {
                commandDescription = [NSString stringWithFormat:@"Fly to %@", [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex + 1] title]];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToAnotherStarSystem:
        {
            commandDescription = [NSString stringWithFormat:@"Fly to another star system?"];
            break;
        }
            
        case SPIPlayerSpaceshipCommandDestroySpaceobject:
        {
            commandDescription = [NSString stringWithFormat:@"Destroy %@", [self.currentSpaceObject title]];
            break;        }
            
        case  SPIPlayerSpaceshipCommandExit: {
            commandDescription = [NSString stringWithFormat:@"Exit"];
            break;
        }
            
            
        default:
            break;
    }
    return commandDescription;
}


@end
