//
//  SPIStar.h
//  Lection2
//
//  Created by kravinov on 11/4/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"

// Классификация звезд
typedef NS_ENUM(NSUInteger, SPIClassOfStar){
    O = 1,
    B,
    A,
    F,
    G,
    K,
    M
};
// ---

@interface SPIStar : SPISpaceObject

@property (nonatomic, assign) SPIClassOfStar classOfStar;
@property (nonatomic, assign) NSNumber* massOfStar;

- (instancetype)initWithName:(NSString *)name;

@end
