//
//  SpaceObject.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 05/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"

@implementation SPISpaceObject

+ (NSString *)typeStringForType:(SPISpaceObjectType)type {
    switch (type) {
        case SPISpaceObjectTypePlanet:
            return @"Planet";
        case SPISpaceObjectTypeAsteroidField:
            return @"Asteroid field";
        // Звезда
        case SPISpaceObjectTypeStar:
            return @"Star";
        // ---
        default:
            return @"Unknown";
    }
}

- (instancetype)initWithType:(SPISpaceObjectType)type name:(NSString *)name {
    self = [super init];
    if (self) {        
        _type = type;
        _name = name;
    }
    return self;
}
- (NSString *)title {
    return [NSString stringWithFormat:@"%@ %@", [SPISpaceObject typeStringForType:self.type], self.name];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ named %@. Turn %ld", [SPISpaceObject typeStringForType:self.type], self.name, (long)self.turn];
}

- (void)nextTurn {
    self.turn++;
}



@end
