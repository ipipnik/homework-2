//
//  SPIStar.m
//  Lection2
//
//  Created by kravinov on 11/4/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIStar.h"

@implementation SPIStar

- (instancetype)initWithName:(NSString *)name;{
    self = [super initWithType:SPISpaceObjectTypeStar name:name];
    if (self) {
        
    }
    return self;
}


- (void)nextTurn {
    [super nextTurn];
    
    self.massOfStar =  @(arc4random() % 6 + 1);
}

- (NSString*)description{
    return [NSString stringWithFormat:@"\nStar: %@\nClass of the Star: %@\nMass of the star: %@",
            self.name,[SPIStar typeStringForType: self.classOfStar], self.massOfStar];
}

// Для удобства задания класса звезды
+ (NSString *)typeStringForType:(SPIClassOfStar ) class {
    
    switch (class) {
        case O:
            return @"0";
        case B:
            return @"B";
        case A:
            return @"A";
        case F:
            return @"F";
        case G:
            return @"G";
        case K:
            return @"K";
        case M:
            return @"M";

        default:
            return @"Unknown";
    }
}
// ---

@end
