//
//  PlayerSpaceship.h
//  Lection2
//
//  Created by Vladislav Grigoriev on 17/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"
#import "SPIStarSystem.h"
#import "SPIGameObject.h"
#import "SPIStar.h"
#import "SPIPlanet.h"
#import "SPIAsteroidField.h"

typedef NS_ENUM(NSInteger, SPIPlayerSpaceshipCommand) {
    SPIPlayerSpaceshipCommandFlyToPreviousObject = 1,
    SPIPlayerSpaceshipCommandExploreCurrentObject,
    SPIPlayerSpaceshipCommandFlyToNextObject,
    SPIPlayerSpaceshipCommandDestroySpaceobject,
    SPIPlayerSpaceshipCommandFlyToAnotherStarSystem,
    SPIPlayerSpaceshipCommandExit = 42,
};
typedef NS_ENUM(NSInteger, SPIPlayerSpaceshipResponse) {
    SPIPlayerSpaceshipResponseOK = 0,
    SPIPlayerSpaceshipResponseExit,
};

@interface SPIPlayerSpaceship : NSObject <SPIGameObject>

@property (nonatomic, strong, readonly) NSString *name;

@property (nonatomic, assign) SPIPlayerSpaceshipCommand nextCommand;
@property (nonatomic, weak, setter=loadStarSystem:) SPIStarSystem *starSystem;
@property (nonatomic, weak) SPISpaceObject *currentSpaceObject;

- (instancetype)initWithName:(NSString *)name;

- (NSString *)availableCommands;

- (SPIPlayerSpaceshipResponse)waitForCommand;

@end
